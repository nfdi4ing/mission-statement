<!-- in this file please add authors, please stick to the syntax to allow machine processing! -->

* Axel Klinger <axel.klinger@tib.eu> https://orcid.org/0000-0001-6442-3510
* Gerald Jagusch <gerald.jagusch@tu-darmstadt.de> https://orcid.org/0000-0001-9964-1112
* Hanna Führ <fuehr@itc.rwth-aachen.de>
* Marius Politze <politze@itc.rwth-aachen.de> https://orcid.org/0000-0003-3175-0659
* Rainer Stotzka <rainer.stotzka@kit.edu> https://orcid.org/0000-0003-3642-1264
