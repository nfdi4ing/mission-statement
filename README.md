# NFDI4Ing Mission Statement Open Document

This is an open document describing the NFDI4Ing Mission statement. Everyone participating in NFDI4Ing is intvited to contribute and discuss on the further development of this document. 

The document should focus on the following points:
* Overarching goals :heavy_check_mark:
* Principles to fulfil the mission :heavy_check_mark:
* Technical & Semantic interoperability :heavy_check_mark:
* Technical specification
* Base functionalities

Recommendations for the technical specification will be formulated progressively in coopertion with the NFDI Section "Common Infrastructure".

The main document is in the file [`mission-statement.md`](mission-statement.md).

The document has several placeholders (:speech_balloon:) to start discussions on individual sections. 

Generated documents
* [HTML](https://nfdi4ing.pages.rwth-aachen.de/mission-statement/index.html)
* [PDF](https://nfdi4ing.pages.rwth-aachen.de/mission-statement/mission-statement.pdf)
* [EBOOK](https://nfdi4ing.pages.rwth-aachen.de/mission-statement/mission-statement.epub)

## Contributing

You can contribute to the document by 
* Contribute to an open [discussion on the different sections](https://git.rwth-aachen.de/nfdi4ing/mission-statement/-/issues).
* [Opening an issue](https://git.rwth-aachen.de/nfdi4ing/mission-statement/-/issues/new) to start a discussion in this repository.
* [Forking](https://git.rwth-aachen.de/nfdi4ing/mission-statement/-/forks/new) the repository and providing a merge request.

If you made a contribution to this document, make sure to be added to the [list of authors](AUTHORS.md).

## License

The text is licensed under under [Creative Commons Attribution 4.0 International License](https://creativecommons.org/licenses/by/4.0/).

